part of responsive_tools;

class ResponsiveRichText extends StatelessWidget {
  final List<ResponsiveText> children;
  final Key? richTextKey;

  ResponsiveRichText({required this.children, this.richTextKey});

  @override
  Widget build(BuildContext context) {
    return RichText(
      key: richTextKey,
      text: TextSpan(
        children: [
          for (ResponsiveText child in children)
            _buildResponsiveTextSpan(
              child,
              getDeviceFontSize(child.size ?? TextSize.M, context),
              context,
            )
        ],
      ),
    );
  }

  TextSpan _buildResponsiveTextSpan(ResponsiveText responsiveText, double fontSize, BuildContext context) {
    TextStyle style = responsiveText.textStyle.copyWith(
      fontSize: fontSize,
      color: responsiveText.color ?? Theme.of(context).textTheme.bodyText1!.color,
      fontFamily: responsiveText.fontFamily ?? Theme.of(context).textTheme.bodyText1!.fontFamily,
    );
    return TextSpan(
      semanticsLabel: responsiveText.semanticsLabel,
      text: responsiveText.text,
      style: style,
    );
  }
}
